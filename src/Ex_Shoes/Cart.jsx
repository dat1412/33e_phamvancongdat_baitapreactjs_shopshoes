import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.dataCart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.quantity}$</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="hinhAnh" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.hanleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="px-3">{item.quantity}</span>
            <button
              onClick={() => {
                this.props.hanleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
