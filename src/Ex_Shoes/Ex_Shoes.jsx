import React, { Component } from "react";
import Cart from "./Cart";
import { data_shoes } from "./data_shoes";
import Item_Shoes from "./Item_Shoes";

export default class Ex_Shoes extends Component {
  state = {
    shoes: data_shoes,
    cart: [],
  };

  // Render danh sách sản phẩm
  renderContent = () => {
    return this.state.shoes.map((item, index) => {
      return (
        <Item_Shoes
          key={index}
          data={item}
          handleAddToCart={this.handleAddToCart}
        />
      );
    });
  };

  // Thêm sản phẩm vào giỏ hàng
  handleAddToCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    let cloneCart = [...this.state.cart];

    if (index == -1) {
      let newProduct = { ...shoe, quantity: 1 };
      cloneCart.push(newProduct);
    } else {
      cloneCart[index].quantity++;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  // Thay đổi số lượng sản phẩm trong giỏ hàng
  hanleChangeQuantity = (idShoe, value) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index == -1) {
      return;
    }
    let cloneCart = [...this.state.cart];
    cloneCart[index].quantity += value;
    if (cloneCart[index].quantity == 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <Cart
          dataCart={this.state.cart}
          hanleChangeQuantity={this.hanleChangeQuantity}
        />
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
