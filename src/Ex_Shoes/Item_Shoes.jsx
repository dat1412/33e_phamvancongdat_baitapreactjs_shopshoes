import React, { Component } from "react";

export default class Item_Shoes extends Component {
  render() {
    let { name, image } = this.props.data;
    return (
      <div className="col-3 mt-4">
        <div className="card" style={{ width: "100%" }}>
          <img src={image} className="card-img-top" alt="image" />
          <div className="card-body">
            <h6 className="card-title ">{name}</h6>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-primary"
            >
              <span className="mr-2">Add to cart</span>
              <i className="fa fa-cart-plus"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
